from django.shortcuts import render, redirect
from receipts.models import Receipt


# Create your views here.
def show_receipts(request):
    if request.user.is_anonymous:
        return redirect("login")
    showreceipts = Receipt.objects.filter(purchaser=request.user)
    context = {"show": showreceipts}
    return render(request, "receipts/list.html", context)
