from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label="username", max_length=150, required=True)
    password = forms.CharField(
        label="Password", widget=forms.PasswordInput, required=True
    )

    fields = ["username", "passsword"]


class SignupForm(forms.Form):
    username = forms.CharField(label="username", max_length=150, required=True)
    password = forms.CharField(
        label="Password",
        max_length=150,
        widget=forms.PasswordInput,
        required=True,
    )
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput, required=True
    )
